File {
  Grid= "@tdr@"
  Plot= "@tdrdat@"
  Current= "@plot@"
  Output= "@log@"
  Parameter= "@parameter@"
}

Electrode {
  { Name="top" Voltage= 0.0 }
  { Name="bottom" Voltage= 0.0 }
}

Physics {

  #if [string compare @SIM@ "Breakdown"] == 0
    Recombination (
      SRH(TempDependence)
      Auger
      Avalanche (OkutoCrowell)
    )
  #elif [string compare @SIM@ "IV"] == 0
    Recombination (
      SRH(DopingDependence TempDependence)
      Auger
    )   
  #endif
   
#  Aniso (
#    Avalanche
#    eMobilityFactor (Total) = 1
#    hMobilityFactor (Total) = 0.83
#  )
  
  Mobility ( 
    HighFieldSaturation
  )

  EffectiveIntrinsicDensity ( oldSlotboom NoFermi )
  Temperature= 300

}  # Physics

Plot {
  eDensity hDensity
  eCurrent hCurrent
  ElectricField
  eQuasiFermi hQuasiFermi
  egradQuasiFermi hgradQuasiFermi
  Potential Doping SpaceCharge
  SRH Auger
  AvalancheGeneration
  eAvalanche hAvalanche
  eMobility hMobility
  DonorConcentration AcceptorConcentration
  Doping
  eVelocity hVelocity
  ConductionBandEnergy ValenceBandEnergy BandGap
}

Math {
  Extrapolate
  ErrRef(electron)= 1
  ErrRef(hole)= 1
  eDrForceRefDens= 1
  hDrForceRefDens= 1
  ExtendedPrecision
  RHSmax= 1e30
  RHSmin= 1e-30
  RHSFactor= 1e30
  CdensityMin= 1.0e-30
  Notdamped= 1000
  Iterations= 15
  TensorGridAniso
}

Solve {
  Poisson
  Coupled( Iterations = 100 LineSearchDamping= 1e-4) { Poisson Electron }
  Coupled( Iterations = 100 LineSearchDamping= 1e-4) { Poisson Electron Hole } 

  #if @<[string compare SIM "Breakdown"] == 0>@
    NewCurrentFile = "BV_"
    Quasistationary (
      Initialstep = 1e-3 Increment = 1.4 
      Maxstep = 0.075 Minstep = 1.e-12
      Goal { Name="top" Voltage= -10000 }
    ) { Coupled { Poisson Electron Hole } }

  #elif @<[string compare SIM "IV"] == 0>@
    Quasistationary (
      Initialstep = 1e-3 Increment = 1.4
      Maxstep = 0.01 Minstep= 1.e-8 
      Goal { Name="top" Voltage= -1 }
    ) { Coupled { Poisson Electron Hole } }

    NewCurrentFile= "IV_"
    Quasistationary (
      Initialstep = 1e-3 Increment = 1.4
      Maxstep = 1.0 Minstep = 1.e-8 
      Goal { Name="top" Voltage= -100.0 }
    ) { Coupled { Poisson Electron Hole } 
        CurrentPlot ( Time= ( Range= (0.00 0.50) Intervals= 20;
          Range= (0.50 0.75) Intervals= 20;
          Range= (0.75 1.00) Intervals= 10)
        )
      }
  #endif
}







