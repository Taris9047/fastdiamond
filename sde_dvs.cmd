
(define Wtot @Width@)
(define Ltot @Thickness@)
(define SubDop 1E12)
(define Loffset 5.0)
(define Woffset 20.0)

; =================== Outer Boundary Creation =========================
;-- Diamond Region
(sdegeo:create-rectangle (position 0.0 0.0 0.0) (position Wtot Ltot 0.0) 
 "FastDiamond" "FastDiamondRegion" )


; =================== Contact Definition Placement  =========================
(sdegeo:define-contact-set "top" 4.0 (color:rgb 1.0 0.0 0.0) "##")
(sdegeo:define-contact-set "bottom"    4.0 (color:rgb 1.0 0.0 0.0) "##")

; For contact regions...
; Contacts are Platinum. Will decide if they are schottky or not later.
(sdegeo:insert-vertex (position (/ Wtot 2.0) 0.0 0.0))
(sdegeo:insert-vertex (position (/ Wtot 2.0) Ltot 0.0))

(sdegeo:define-2d-contact (find-edge-id (position (/ Wtot 2.0) 0.0 0.0)) "top")
(sdegeo:define-2d-contact (find-edge-id (position (/ Wtot 2.0) Ltot 0.0)) "bottom")

; =================== Constant and Analytical Profiles =========================
;---Definitions
(sdedr:define-constant-profile "Sub_Dop_Defn" "PhosphorusActiveConcentration" SubDop)

;---Windows
(sdedr:define-refeval-window "Sub_Win" "Rectangle"  (position 0 0 0) (position Wtot Ltot 0.0)) 

;---Placement
;(sdedr:define-constant-profile-placement "Sub_Place" "Sub_Dop_Defn" "Sub_Win")

; -- Edge termination extension implant specification
;(sdedr:define-gaussian-profile "Impl.Res"
; "BoronActiveConcentration"
; "PeakPos" 0.14  "PeakVal" 3.9e19
; "ValueAtDepth"  1e16 "Depth" 0.42
; "Gauss"  "Factor" 1.0
;)

; -- implant placement
(sdedr:define-refinement-window "BaseLine" "Line"  
 (position (/ Wtot 2.0) 0.0 0.0)
 (position Wtot 0.0 0.0) )
; -- implant definition

(sdedr:define-analytical-profile-placement 
  "Impl" "BaseLine" "Positive" "NoReplace" "Eval")


; =================== Refinements  =========================

;---Definitions
(sdedr:define-refinement-size "Global_Ref_Defn" 
  20.0 10.0 0.0 
  5.0 2.5 0.0 )
(sdedr:define-multibox-size   "Multi_Box_Defn"
  5.0 2.5 0.0 
  0.5 0.2 0.0 
  3.0 1.0 0.0)
(sdedr:define-refinement-size "Ref.Contact" 
  2.0 1.0 0.0
  1.0 0.5 0.0 )

;---Windows
(sdedr:define-refinement-window  "Global_Win" 
 "Rectangle"  (position 0.0 0.0 0.0) (position Wtot Ltot 0.0) )
(sdedr:define-refinement-window  "Multi_Box_Win"
 "Rectangle" (position Woffset Loffset 0.0) (position (- Wtot Woffset) (- Ltot Loffset) 0.0) )
(sdedr:define-refinement-window "RWin.Top" 
 "Rectangle"   (position   0.0 0 0.0)  (position  Wtot  Loffset  0.0) )
(sdedr:define-refinement-window "RWin.Bottom" 
 "Rectangle"   (position   0.0 (- Ltot Loffset) 0.0)  (position  Wtot Ltot 0.0) )

;---Refinement function
(sdedr:define-refinement-function "Ref.Contact" "DopingConcentration" "MaxTransDiff" 1)


;---Placement
(sdedr:define-refinement-placement "Global_Ref_Place" "Global_Ref_Defn" "Global_Win" )
(sdedr:define-refinement-placement "RefPlace.Top" "Ref.Contact" "RWin.Top")
(sdedr:define-refinement-placement "RefPlace.Bottom" "Ref.Contact" "RWin.Bottom")
(sdedr:define-multibox-placement "Multi_Box_Place"  "Multi_Box_Defn" "Multi_Box_Win")

;----------------------------------------------------------------------
; Build Mesh 
; Added -a -o option to avoid floating point error. --> This seems to be a bug...
(sde:build-mesh "snmesh" "-a -o" "n@node@_msh")

