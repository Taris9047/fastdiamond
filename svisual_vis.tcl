#----------------------------------------------------------------------#
lib::SetInfoDef 1
#----------------------------------------------------------------------#

set N     @node@
set i     @node:index@ 

#- Automatic alternating color assignment tied to node index
#----------------------------------------------------------------------#
set COLORS  [list green blue red orange magenta violet brown]
set NCOLORS [llength $COLORS]
set color   [lindex  $COLORS [expr $i%$NCOLORS]]

if {[lsearch [list_plots] Plot_IV] == -1} {
	create_plot -1d -name Plot_IV
	set_plot_prop -title "I-V Characteristics" -title_font_size 20
}
select_plots Plot_IV

#if @<[string compare SIM "IV"] == 0>@
set_axis_prop -axis y -title {Current [A/<greek>m</greek>m]} \
	-title_font_size 16 -scale_font_size 14 -type linear
set_axis_prop -axis x -title {Voltage [V]} \
	-title_font_size 16 -scale_font_size 14 -type linear 

load_file IV_@plot@ -name PLT($N)
set_legend_prop -font_size 12 -location top_left -font_att bold

create_curve -name IV($N) -dataset PLT($N) \
	-axisX "top_schottky OuterVoltage" -axisY "top TotalCurrent"
set_curve_prop IV($N) -label "forward-bias IV" \
	-color $color -line_style solid -line_width 3

#else
load_file BV_@plot@ -name PLT($N)

set_legend_prop -font_size 12 -location top_right -font_att bold
create_curve -name IV($N) -dataset PLT($N) \
	-axisX "top_schottky InnerVoltage" -axisY "top TotalCurrent"
set_curve_prop IV($N) -label "reverse-bias IV" \
	-color $color -line_style solid -line_width 3

set_axis_prop -axis y -title {Current [A/<greek>m</greek>m]} \
	-title_font_size 16 -scale_font_size 14 -type log -range {1e-18 1e-9}
set_axis_prop -axis x -title {Voltage [V]} \
	-title_font_size 16 -scale_font_size 14 -type linear 

echo "Extracting Breakdown voltage"

#- Storing variables in Tcl lists for parameter extraction
#----------------------------------------------------------------------#
set Vs [get_variable_data "top_schottky InnerVoltage" -dataset PLT($N)]
set Is [get_variable_data "top_schottky TotalCurrent" -dataset PLT($N)]

ext::ExtractBVv -out BVv -name out -v $Vs -i $Is -sign -1
echo "BVv is [format %.3e $BVv] V"

ext::ExtractBVi -out BVi -name out -v $Vs -i $Is -io 1e-10
echo "BVi is [format %.3e $BVi] V"
#endif
